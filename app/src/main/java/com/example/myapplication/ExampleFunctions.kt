package com.example.myapplication

interface ExampleFunctions {
    fun incX()
    fun decX()
    fun incY()
    fun decY()
    fun setP(p: Boolean)
    fun setQ(q: Boolean)
}