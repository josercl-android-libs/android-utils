package com.example.myapplication.livedata

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.ExampleFunctions
import josercl.androidutils.livedata.StringPreferenceLiveData
import josercl.androidutils.livedata.and
import josercl.androidutils.livedata.div
import josercl.androidutils.livedata.minus
import josercl.androidutils.livedata.minusAssign
import josercl.androidutils.livedata.not
import josercl.androidutils.livedata.or
import josercl.androidutils.livedata.plus
import josercl.androidutils.livedata.plusAssign
import josercl.androidutils.livedata.readOnly
import josercl.androidutils.livedata.times

class LiveDataViewModel(
    preferences: SharedPreferences
): ViewModel(), ExampleFunctions {
    private val _x = MutableLiveData(0f)
    val x = _x.readOnly()

    private val _y = MutableLiveData(0f)
    val y = _y.readOnly()

    private val _p = MutableLiveData(false)
    val p = _p.readOnly()

    private val _q = MutableLiveData(false)
    val q = _q.readOnly()

    val pref = StringPreferenceLiveData(preferences, "PREF_STRING", "")

    val and = p and q
    val or  = p or  q
    val notP = !p
    val notQ = !q

    val sum = x + y
    val minus = x - y
    val mult = x * y
    val div = x / y

    override fun incX() { _x += 1f }
    override fun decX() { _x -= 1f }
    override fun incY() { _y += 1f }
    override fun decY() { _y -= 1f }

    override fun setP(p: Boolean) = _p.postValue(p)

    override fun setQ(q: Boolean) = _q.postValue(q)
}