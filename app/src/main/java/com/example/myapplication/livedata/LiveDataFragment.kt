package com.example.myapplication.livedata

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.ExampleFragment

class LiveDataFragment: ExampleFragment() {
    private lateinit var viewModel: LiveDataViewModel // by viewModels()

    override fun getTitle(): String = "LiveData Example"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this.viewModelStore, object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return LiveDataViewModel(preferences) as T
            }
        })[LiveDataViewModel::class.java]

        setupUI(viewModel)

        viewModel.x.observe(viewLifecycleOwner) { binding.textX.text = "$it" }
        viewModel.y.observe(viewLifecycleOwner) { binding.textY.text = "$it" }
        viewModel.sum.observe(viewLifecycleOwner) { binding.textSum.text = "$it" }
        viewModel.minus.observe(viewLifecycleOwner) { binding.textMinus.text = "$it" }
        viewModel.mult.observe(viewLifecycleOwner) { binding.textMult.text = "$it" }
        viewModel.div.observe(viewLifecycleOwner) { binding.textDiv.text = "$it" }
        viewModel.p.observe(viewLifecycleOwner, this::updatePButton)
        viewModel.q.observe(viewLifecycleOwner, this::updateQButton)
        viewModel.and.observe(viewLifecycleOwner) { binding.textAnd.text = "$it" }
        viewModel.or.observe(viewLifecycleOwner) { binding.textOr.text = "$it" }
        viewModel.notP.observe(viewLifecycleOwner) { binding.textNot.text = "$it" }
        viewModel.notQ.observe(viewLifecycleOwner) { binding.textNotQ.text = "$it" }
        viewModel.pref.observe(viewLifecycleOwner) { binding.textPref.text = it }
    }
}