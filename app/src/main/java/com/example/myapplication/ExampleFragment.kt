package com.example.myapplication

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.example.myapplication.databinding.FragmentMainBinding
import josercl.androidutils.core.getColorPrimary
import josercl.androidutils.core.getColorSecondary
import java.util.Date

abstract class ExampleFragment: Fragment(R.layout.fragment_main) {
    protected val preferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext().applicationContext)
    }
    protected lateinit var binding: FragmentMainBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBinding.bind(view)
    }

    protected fun setupUI(functions: ExampleFunctions) {
        binding.exampleName.text = getTitle()

        binding.view1.setBackgroundColor(requireContext().getColorPrimary())
        binding.view2.setBackgroundColor(requireContext().getColorSecondary())

        binding.xplus1.setOnClickListener { functions.incX() }
        binding.xminus1.setOnClickListener { functions.decX() }
        binding.yplus1.setOnClickListener { functions.incY() }
        binding.yminus1.setOnClickListener { functions.decY() }

        binding.textP.addOnCheckedChangeListener { _, isChecked -> functions.setP(isChecked) }
        binding.textQ.addOnCheckedChangeListener { _, isChecked -> functions.setQ(isChecked) }

        binding.changePrefBtn.setOnClickListener {
            preferences.edit(true) {
                putString("PREF_STRING", Date().toString())
            }
        }
    }

    protected fun updatePButton(value: Boolean) {
        binding.textP.isChecked = value
        binding.textP.text = "$value"
    }

    protected fun updateQButton(value: Boolean) {
        binding.textQ.isChecked = value
        binding.textQ.text = "$value"
    }

    abstract fun getTitle(): String
}