package com.example.myapplication.flow

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.myapplication.ExampleFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class FlowFragment : ExampleFragment() {
    private lateinit var viewModel: FlowViewModel

    override fun getTitle(): String = "Flow Example"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return FlowViewModel(preferences) as T
            }
        })[FlowViewModel::class.java]

        setupUI(viewModel)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch { viewModel.x.collect { binding.textX.text = "$it" } }
                launch { viewModel.y.collect { binding.textY.text = "$it" } }
                launch { viewModel.sum.collect { binding.textSum.text = "$it" } }
                launch { viewModel.minus.collect { binding.textMinus.text = "$it" } }
                launch { viewModel.mult.collect { binding.textMult.text = "$it" } }
                launch { viewModel.div.collect { binding.textDiv.text = "$it" } }
                launch { viewModel.p.collect(this@FlowFragment::updatePButton) }
                launch { viewModel.q.collect(this@FlowFragment::updateQButton) }
                launch { viewModel.and.collect { binding.textAnd.text = "$it" } }
                launch { viewModel.or.collect { binding.textOr.text = "$it" } }
                launch { viewModel.notP.collect { binding.textNot.text = "$it" } }
                launch { viewModel.notQ.collect { binding.textNotQ.text = "$it" } }
                launch { viewModel.pref.collect { binding.textPref.text = it } }
            }
        }
    }
}