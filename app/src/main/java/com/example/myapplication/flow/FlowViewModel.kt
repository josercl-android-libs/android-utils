package com.example.myapplication.flow

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import com.example.myapplication.ExampleFunctions
import josercl.androidutils.flow.and
import josercl.androidutils.flow.div
import josercl.androidutils.flow.minus
import josercl.androidutils.flow.minusAssign
import josercl.androidutils.flow.not
import josercl.androidutils.flow.or
import josercl.androidutils.flow.plus
import josercl.androidutils.flow.plusAssign
import josercl.androidutils.flow.preferenceFlow
import josercl.androidutils.flow.times
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class FlowViewModel(
    preferences: SharedPreferences
) : ViewModel(), ExampleFunctions {
    val pref = preferenceFlow(preferences, "PREF_STRING", "")

    private val _x = MutableStateFlow(0f)
    val x = _x.asStateFlow()

    private val _y = MutableStateFlow(0f)
    val y = _y.asStateFlow()

    private val _p = MutableStateFlow(false)
    val p = _p.asStateFlow()

    private val _q = MutableStateFlow(false)
    val q = _q.asStateFlow()

    val sum = x + y
    val minus = x - y
    val mult = x * y
    val div = x / y

    val and = p and q
    val or = p or q
    val notP = !p
    val notQ = !q

    override fun incX() {
        _x += 1f
    }

    override fun decX() {
        _x -= 1f
    }

    override fun incY() {
        _y += 1f
    }

    override fun decY() {
        _y -= 1f
    }

    override fun setP(p: Boolean) = _p.update { p }

    override fun setQ(q: Boolean) = _q.update { q }
}