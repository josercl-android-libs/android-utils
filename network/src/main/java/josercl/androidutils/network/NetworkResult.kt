package josercl.androidutils.network

sealed interface NetworkResult<out T> {
    data class Success<T>(val data: T) : NetworkResult<T>
    data class Error(val exception: Throwable? = null) : NetworkResult<Nothing>
    object Loading : NetworkResult<Nothing>
}