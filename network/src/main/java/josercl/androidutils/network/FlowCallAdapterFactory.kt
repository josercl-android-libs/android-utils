package josercl.androidutils.network

import android.util.Log
import kotlinx.coroutines.flow.Flow
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class FlowCallAdapterFactory : CallAdapter.Factory() {
    companion object {
        @JvmStatic
        fun create() = FlowCallAdapterFactory()
    }

    override fun get(returnType: Type, annotations: Array<out Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        if (getRawType(returnType) != Flow::class.java) {
            return null
        }

        check(returnType is ParameterizedType)

        val responseType = getParameterUpperBound(0, returnType)

        return FlowCallAdapter<Any>(responseType)
    }

}