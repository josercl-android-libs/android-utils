package josercl.androidutils.network

import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type

class FlowCallAdapter<T>(
    private val responseType: Type
) : CallAdapter<T, Flow<T>> {

    override fun responseType(): Type = responseType

    override fun adapt(call: Call<T>): Flow<T> {
        return callbackFlow {

            call.enqueue(object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    trySend(response.body()!!)
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    close(t)
                }
            })

            awaitClose { call.cancel() }
        }
    }
}

