package josercl.androidutils.flow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import josercl.androidutils.network.NetworkResult

fun <T> Flow<T>.asResult(): Flow<NetworkResult<T>> =
    this.map<T, NetworkResult<T>> {
        NetworkResult.Success(it)
    }.onStart {
        emit(NetworkResult.Loading)
    }.catch {
        emit(NetworkResult.Error(it))
    }