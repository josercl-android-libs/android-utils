package josercl.androidutils.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData

operator fun MutableLiveData<Float>.plusAssign(n: Float) = this.postValue(value?.plus(n))
operator fun MutableLiveData<Float>.minusAssign(n: Float) = this.postValue(value?.minus(n))
operator fun MutableLiveData<Float>.timesAssign(n: Float) = this.postValue(value?.times(n))
operator fun MutableLiveData<Float>.divAssign(n: Float) = this.postValue(value?.div(n))

infix operator fun LiveData<Float>.plus(other: LiveData<Float>) : MediatorLiveData<Float> {
    val result = MediatorLiveData<Float>()

    result.addSource(this) {
        result.value = it + (other.value ?: 0f)
    }

    result.addSource(other) {
        result.value = (value ?: 0f) + it
    }

    return result
}

infix operator fun LiveData<Float>.minus(other: LiveData<Float>) : MediatorLiveData<Float> {
    val result = MediatorLiveData<Float>()

    result.addSource(this) {
        result.value = it - (other.value ?: 0f)
    }

    result.addSource(other) {
        result.value = (value ?: 0f) - it
    }

    return result
}

infix operator fun LiveData<Float>.times(other: LiveData<Float>) : MediatorLiveData<Float> {
    val result = MediatorLiveData<Float>()

    result.addSource(this) {
        result.value = it * (other.value ?: 0f)
    }

    result.addSource(other) {
        result.value = (value ?: 0f) * it
    }

    return result
}

infix operator fun LiveData<Float>.div(other: LiveData<Float>) : MediatorLiveData<Float> {
    val result = MediatorLiveData<Float>()

    result.addSource(this) {
        val otherValue = other.value ?: 0f
        result.value = it.div(otherValue)
    }

    result.addSource(other) {
        result.value = (value ?: 0f).div(it)
    }

    return result
}