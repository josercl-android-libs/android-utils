package josercl.androidutils.livedata

import android.content.SharedPreferences

class StringPreferenceLiveData(
    prefs: SharedPreferences,
    key: String,
    defValue: String
) : PreferenceLiveData<String>(prefs, key, defValue) {

    override fun getValueFromPreference(key: String): String = prefs.getString(key, defaultValue)!!
}

class IntPreferenceLiveData(
    prefs: SharedPreferences,
    key: String,
    defValue: Int
) : PreferenceLiveData<Int>(prefs, key, defValue) {

    override fun getValueFromPreference(key: String): Int = prefs.getInt(key, defaultValue)
}

class FloatPreferenceLiveData(
    prefs: SharedPreferences,
    key: String,
    defValue: Float
) : PreferenceLiveData<Float>(prefs, key, defValue) {

    override fun getValueFromPreference(key: String): Float = prefs.getFloat(key, defaultValue)
}

class LongPreferenceLiveData(
    prefs: SharedPreferences,
    key: String,
    defValue: Long
) : PreferenceLiveData<Long>(prefs, key, defValue) {

    override fun getValueFromPreference(key: String): Long = prefs.getLong(key, defaultValue)
}

class BooleanPreferenceLiveData(
    prefs: SharedPreferences,
    key: String,
    defValue: Boolean
) : PreferenceLiveData<Boolean>(prefs, key, defValue) {

    override fun getValueFromPreference(key: String): Boolean = prefs.getBoolean(key, defaultValue)
}