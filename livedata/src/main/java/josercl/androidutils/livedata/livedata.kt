package josercl.androidutils.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map

fun <T> MutableLiveData<T>.asLiveData(): LiveData<T> = this
fun <T> MutableLiveData<T>.readOnly(): LiveData<T> = this

fun <T> LiveData<T>.isNull(): LiveData<Boolean> = this.map { it == null }
fun <T> LiveData<T>.isNotNull(): LiveData<Boolean> = this.map { it != null }