package josercl.androidutils.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData

operator fun MutableLiveData<Int>.plusAssign(n: Int) = this.postValue(value?.plus(n))
operator fun MutableLiveData<Int>.minusAssign(n: Int) = this.postValue(value?.minus(n))
operator fun MutableLiveData<Int>.timesAssign(n: Int) = this.postValue(value?.times(n))
operator fun MutableLiveData<Int>.divAssign(n: Int) = this.postValue(value?.div(n))

infix operator fun LiveData<Int>.plus(other: LiveData<Int>): MediatorLiveData<Int> {
    val result = MediatorLiveData<Int>()

    result.addSource(this) {
        result.value = it + (other.value ?: 0)
    }

    result.addSource(other) {
        result.value = (value ?: 0) + it
    }

    return result
}

infix operator fun LiveData<Int>.minus(other: LiveData<Int>): MediatorLiveData<Int> {
    val result = MediatorLiveData<Int>()

    result.addSource(this) {
        result.value = it - (other.value ?: 0)
    }

    result.addSource(other) {
        result.value = (value ?: 0) - it
    }

    return result
}

infix operator fun LiveData<Int>.times(other: LiveData<Int>): MediatorLiveData<Int> {
    val result = MediatorLiveData<Int>()

    result.addSource(this) {
        result.value = it * (other.value ?: 0)
    }

    result.addSource(other) {
        result.value = (value ?: 0) * it
    }

    return result
}

infix operator fun LiveData<Int>.div(other: LiveData<Int>): MediatorLiveData<Int> {
    val result = MediatorLiveData<Int>()

    result.addSource(this) {
        val otherValue = other.value ?: 0
        if (otherValue != 0) {
            result.value = it / otherValue
        } else {
            result.value = 0
        }
    }

    result.addSource(other) {
        if (it != 0) {
            result.value = (value ?: 0) / it
        } else {
            result.value = 0
        }
    }

    return result
}