package josercl.androidutils.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.map

fun <T> LiveData<List<T>>.isEmpty(): LiveData<Boolean> = this.map(List<T>::isEmpty)
fun <T> LiveData<List<T>>.isNotEmpty(): LiveData<Boolean> = this.map(List<T>::isNotEmpty)
fun <T> LiveData<List<T>>.size(): LiveData<Int> = this.map { it.size }