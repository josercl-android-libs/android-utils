package josercl.androidutils.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.map

operator fun LiveData<Boolean>.not() = this.map(Boolean::not)

infix fun LiveData<Boolean>.and(other: LiveData<Boolean>): MediatorLiveData<Boolean> {
    val mediator = MediatorLiveData<Boolean>()
    mediator.addSource(this) {
        mediator.value = it && other.value ?: false
    }
    mediator.addSource(other) {
        mediator.value = this.value ?: false && it
    }
    return mediator
}

infix fun LiveData<Boolean>.or(other: LiveData<Boolean>): MediatorLiveData<Boolean> {
    val mediator = MediatorLiveData<Boolean>()
    mediator.addSource(this) {
        mediator.value = it || other.value ?: false
    }
    mediator.addSource(other) {
        mediator.value = this.value ?: false || it
    }
    return mediator
}
