package josercl.androidutils.livedata

import android.content.SharedPreferences
import androidx.lifecycle.LiveData

abstract class PreferenceLiveData<T>(
    protected val prefs: SharedPreferences,
    private val key: String,
    protected val defaultValue: T
) : LiveData<T>() {

    private val preferenceChangedListener =
        SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            if (key == this@PreferenceLiveData.key) {
                value = getValueFromPreference(key)
            }
        }

    override fun onActive() {
        super.onActive()
        value = getValueFromPreference(key)
        prefs.registerOnSharedPreferenceChangeListener(preferenceChangedListener)
    }

    override fun onInactive() {
        prefs.unregisterOnSharedPreferenceChangeListener(preferenceChangedListener)
        super.onInactive()
    }

    abstract fun getValueFromPreference(key: String): T
}
