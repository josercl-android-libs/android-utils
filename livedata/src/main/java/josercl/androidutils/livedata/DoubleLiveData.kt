package josercl.androidutils.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData

operator fun MutableLiveData<Double>.plusAssign(n: Double) = this.postValue(value?.plus(n))
operator fun MutableLiveData<Double>.minusAssign(n: Double) = this.postValue(value?.minus(n))
operator fun MutableLiveData<Double>.timesAssign(n: Double) = this.postValue(value?.times(n))
operator fun MutableLiveData<Double>.divAssign(n: Double) = this.postValue(value?.div(n))

infix operator fun LiveData<Double>.plus(other: LiveData<Double>) : MediatorLiveData<Double> {
    val result = MediatorLiveData<Double>()

    result.addSource(this) {
        result.value = it + (other.value ?: 0.0)
    }

    result.addSource(other) {
        result.value = (value ?: 0.0) + it
    }

    return result
}

infix operator fun LiveData<Double>.minus(other: LiveData<Double>) : MediatorLiveData<Double> {
    val result = MediatorLiveData<Double>()

    result.addSource(this) {
        result.value = it - (other.value ?: 0.0)
    }

    result.addSource(other) {
        result.value = (value ?: 0.0) - it
    }

    return result
}

infix operator fun LiveData<Double>.times(other: LiveData<Double>) : MediatorLiveData<Double> {
    val result = MediatorLiveData<Double>()

    result.addSource(this) {
        result.value = it * (other.value ?: 0.0)
    }

    result.addSource(other) {
        result.value = (value ?: 0.0) * it
    }

    return result
}

infix operator fun LiveData<Double>.div(other: LiveData<Double>) : MediatorLiveData<Double> {
    val result = MediatorLiveData<Double>()

    result.addSource(this) {
        val otherValue = other.value ?: 0.0
        if (otherValue != 0.0) {
            result.value = it / otherValue
        } else {
            result.value = 0.0
        }
    }

    result.addSource(other) {
        if (it != 0.0) {
            result.value = (value ?: 0.0) / it
        } else {
            result.value = 0.0
        }
    }

    return result
}