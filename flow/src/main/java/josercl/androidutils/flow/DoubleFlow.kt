package josercl.androidutils.flow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.update

@JvmName("doubleFlowPlusAssign")
operator fun MutableStateFlow<Double>.plusAssign(n: Double) = this.update { it + n }

@JvmName("doubleFlowMinusAssign")
operator fun MutableStateFlow<Double>.minusAssign(n: Double) = this.update { it - n }

@JvmName("doubleFlowTimesAssign")
operator fun MutableStateFlow<Double>.timesAssign(n: Double) = this.update { it * n }

@JvmName("doubleFlowDivAssign")
operator fun MutableStateFlow<Double>.divAssign(n: Double) = this.update { it / n }

@JvmName("doubleFlowPlus")
infix operator fun Flow<Double>.plus(other: Flow<Double>): Flow<Double> = this.combine(other) { a, b -> a + b }

@JvmName("doubleFlowMinus")
infix operator fun Flow<Double>.minus(other: Flow<Double>): Flow<Double> = this.combine(other) { a, b -> a - b }

@JvmName("doubleFlowTimes")
infix operator fun Flow<Double>.times(other: Flow<Double>): Flow<Double> = this.combine(other) { a, b -> a * b }

@JvmName("doubleFlowDiv")
infix operator fun Flow<Double>.div(other: Flow<Double>): Flow<Double> = this.combine(other) { a, b -> a / b }