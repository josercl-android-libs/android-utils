package josercl.androidutils.flow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

fun <T> Flow<List<T>>.isEmpty() = this.map(List<T>::isEmpty)
fun <T> Flow<List<T>>.isNotEmpty() = this.map(List<T>::isNotEmpty)
fun <T> Flow<List<T>>.size(): Flow<Int> = this.map { it.size }