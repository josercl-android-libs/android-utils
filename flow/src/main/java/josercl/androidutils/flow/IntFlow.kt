package josercl.androidutils.flow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.update

@JvmName("intFlowPlusAssign")
operator fun MutableStateFlow<Int>.plusAssign(n: Int) = this.update { it + n }

@JvmName("intFlowMinusAssign")
operator fun MutableStateFlow<Int>.minusAssign(n: Int) = this.update { it - n }

@JvmName("intFlowTimesAssign")
operator fun MutableStateFlow<Int>.timesAssign(n: Int) = this.update { it * n }

@JvmName("intFlowDivAssign")
operator fun MutableStateFlow<Int>.divAssign(n: Int) = this.update { it / n }

@JvmName("intFlowPlus")
infix operator fun Flow<Int>.plus(other: Flow<Int>): Flow<Int> = this.combine(other) { a, b -> a + b }

@JvmName("intFlowMinus")
infix operator fun Flow<Int>.minus(other: Flow<Int>): Flow<Int> = this.combine(other) { a, b -> a - b }

@JvmName("intFlowTimes")
infix operator fun Flow<Int>.times(other: Flow<Int>): Flow<Int> = this.combine(other) { a, b -> a * b }

@JvmName("intFlowDiv")
infix operator fun Flow<Int>.div(other: Flow<Int>): Flow<Int> = this.combine(other) { a, b -> a / b }