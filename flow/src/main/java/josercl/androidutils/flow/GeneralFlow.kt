package josercl.androidutils.flow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

fun <T> Flow<T>.isNull(): Flow<Boolean> = this.map { it == null }
fun <T> Flow<T>.isNotNull(): Flow<Boolean> = this.map { it != null }