package josercl.androidutils.flow

import android.content.SharedPreferences
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

internal fun <T> preferenceFlowImpl(
    preferences: SharedPreferences,
    key: String,
    getPreferenceValue: () -> T
): Flow<T> {
    return callbackFlow {
        val preferenceChangedListener =
            SharedPreferences.OnSharedPreferenceChangeListener { _, prefKey ->
                if (key == prefKey) {
                    trySendBlocking(getPreferenceValue())
                }
            }

        preferences.registerOnSharedPreferenceChangeListener(preferenceChangedListener)

        preferenceChangedListener.onSharedPreferenceChanged(preferences, key)

        awaitClose {
            preferences.unregisterOnSharedPreferenceChangeListener(preferenceChangedListener)
        }
    }
}

@JvmName("stringPreferenceFlow")
fun preferenceFlow(
    preferences: SharedPreferences,
    key: String,
    defaultValue: String
): Flow<String> = preferenceFlowImpl(preferences, key) {
    preferences.getString(key, defaultValue)!!
}

@JvmName("intPreferenceFlow")
fun preferenceFlow(
    preferences: SharedPreferences,
    key: String,
    defaultValue: Int
): Flow<Int> = preferenceFlowImpl(preferences, key) {
    preferences.getInt(key, defaultValue)
}

@JvmName("longPreferenceFlow")
fun preferenceFlow(
    preferences: SharedPreferences,
    key: String,
    defaultValue: Long
): Flow<Long> = preferenceFlowImpl(preferences, key) {
    preferences.getLong(key, defaultValue)
}

@JvmName("floatPreferenceFlow")
fun preferenceFlow(
    preferences: SharedPreferences,
    key: String,
    defaultValue: Float
): Flow<Float> = preferenceFlowImpl(preferences, key) {
    preferences.getFloat(key, defaultValue)
}

@JvmName("booleanPreferenceFlow")
fun preferenceFlow(
    preferences: SharedPreferences,
    key: String,
    defaultValue: Boolean
): Flow<Boolean> = preferenceFlowImpl(preferences, key) {
    preferences.getBoolean(key, defaultValue)
}