package josercl.androidutils.flow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map

operator fun Flow<Boolean>.not() = this.map(Boolean::not)

infix fun Flow<Boolean>.and(other: Flow<Boolean>) = this.combine(other) { a, b -> a && b }

infix fun Flow<Boolean>.or(other: Flow<Boolean>) = this.combine(other) { a, b -> a || b }
