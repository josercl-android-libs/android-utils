package josercl.androidutils.flow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.update

@JvmName("floatFlowPlusAssign")
operator fun MutableStateFlow<Float>.plusAssign(n: Float) = this.update { it + n }

@JvmName("floatFlowMinusAssign")
operator fun MutableStateFlow<Float>.minusAssign(n: Float) = this.update { it - n }

@JvmName("floatFlowTimesAssign")
operator fun MutableStateFlow<Float>.timesAssign(n: Float) = this.update { it * n }

@JvmName("floatFlowDivAssign")
operator fun MutableStateFlow<Float>.divAssign(n: Float) = this.update { it / n }

@JvmName("floatFlowPlus")
infix operator fun Flow<Float>.plus(other: Flow<Float>): Flow<Float> = this.combine(other) { a, b -> a + b }

@JvmName("floatFlowMinus")
infix operator fun Flow<Float>.minus(other: Flow<Float>): Flow<Float> = this.combine(other) { a, b -> a - b }

@JvmName("floatFlowTimes")
infix operator fun Flow<Float>.times(other: Flow<Float>): Flow<Float> = this.combine(other) { a, b -> a * b }

@JvmName("floatFlowDiv")
infix operator fun Flow<Float>.div(other: Flow<Float>): Flow<Float> = this.combine(other) { a, b -> a / b }