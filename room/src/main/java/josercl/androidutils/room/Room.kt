package josercl.androidutils.room

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

fun migration(
    oldVersion: Int,
    newVersion: Int,
    block: SupportSQLiteDatabase.() -> Unit
): Migration =
    object : Migration(oldVersion, newVersion) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.block()
        }
    }