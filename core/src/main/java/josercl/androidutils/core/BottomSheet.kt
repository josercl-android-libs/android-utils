package josercl.androidutils.core

import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior

fun BottomSheetBehavior<*>.doOnStateChanged(onStateChanged: (bottomSheet: View, newState: Int) -> Unit) {
    addBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            onStateChanged(bottomSheet, newState)
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    })
}

fun BottomSheetBehavior<*>.doOnSlide(onSlide: (bottomSheet: View, offset: Float) -> Unit) {
    addBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {}

        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            onSlide(bottomSheet, slideOffset)
        }
    })
}

fun BottomSheetBehavior<*>?.isExpanded(): Boolean {
    if (this == null) {
        return false
    }
    return this.state == BottomSheetBehavior.STATE_EXPANDED
}
fun BottomSheetBehavior<*>?.isCollapsed(): Boolean {
    if (this == null) {
        return false
    }
    return this.state == BottomSheetBehavior.STATE_COLLAPSED
}

fun BottomSheetBehavior<*>?.expand() {
    this?.state = BottomSheetBehavior.STATE_EXPANDED
}

fun BottomSheetBehavior<*>?.collapse() {
    this?.state = BottomSheetBehavior.STATE_COLLAPSED
}