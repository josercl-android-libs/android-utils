package josercl.androidutils.core

import android.content.Context
import android.util.TypedValue
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.core.content.res.ResourcesCompat

@ColorInt
private fun Context.getThemeColor(@AttrRes color: Int): Int {
    val value = TypedValue()
    this.theme.resolveAttribute(color, value, true)
    return value.data
}

@ColorInt fun Context.getColorPrimary() = this.getThemeColor(com.google.android.material.R.attr.colorPrimary)
@ColorInt fun Context.getColorOnPrimary() = this.getThemeColor(com.google.android.material.R.attr.colorOnPrimary)

@ColorInt fun Context.getColorSecondary() = this.getThemeColor(com.google.android.material.R.attr.colorSecondary)
@ColorInt fun Context.getColorOnSecondary() = this.getThemeColor(com.google.android.material.R.attr.colorOnSecondary)

@ColorInt fun Context.getColorSurface() = this.getThemeColor(com.google.android.material.R.attr.colorSurface)
@ColorInt fun Context.getColorOnSurface() = this.getThemeColor(com.google.android.material.R.attr.colorOnSurface)

@ColorInt fun Context.getColorOnBackground() = this.getThemeColor(com.google.android.material.R.attr.colorOnBackground)

@ColorInt fun Context.getTransparentColor() = ResourcesCompat.getColor(
    resources,
    android.R.color.transparent,
    theme
)