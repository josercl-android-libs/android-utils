package josercl.androidutils.core

import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun Throwable.isNetworkError() =
    this is ConnectException ||
        this is UnknownHostException ||
        this is SocketException ||
        this is SocketTimeoutException
