# Android Utils

## Downloading / Installing

### Maven

Add this to your `pom.xml` file

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/33854648/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/33854648/packages/maven</url>
  </repository>

  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/33854648/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

And then add this to your "dependencies" block

```xml
<!-- For livedata Utils -->
<dependency>
  <groupId>josercl.androidutils</groupId>
  <artifactId>livedata</artifactId>
  <version>1.0.2</version>
</dependency>

<!-- For core Utils -->
<dependency>
  <groupId>josercl.androidutils</groupId>
  <artifactId>core</artifactId>
  <version>1.0.0</version>
</dependency>

<!-- For room Utils -->
<dependency>
  <groupId>josercl.androidutils</groupId>
  <artifactId>room</artifactId>
  <version>1.0.0</version>
</dependency>

<!-- For flow Utils -->
<dependency>
  <groupId>josercl.androidutils</groupId>
  <artifactId>flow</artifactId>
  <version>1.0.3</version>
</dependency>

<!-- For network Utils -->
<dependency>
  <groupId>josercl.androidutils</groupId>
  <artifactId>room</artifactId>
  <version>1.0.0</version>
</dependency>
```

### Gradle (Groovy DSL)

Add the repository

```groovy
maven {
  url 'https://gitlab.com/api/v4/projects/33854648/packages/maven'
}
```

And the dependency(ies)

```groovy
implementation 'josercl.androidutils:core:1.0.0'
implementation 'josercl.androidutils:room:1.0.0'
implementation 'josercl.androidutils:livedata:1.0.3'
implementation 'josercl.androidutils:flow:1.0.3'
```

### Gradle (Kotlin DSL)

Add the repository

```groovy
maven("https://gitlab.com/api/v4/projects/33854648/packages/maven")
```

And the dependency(ies)

```groovy
implementation("josercl.androidutils:core:1.0.0")
implementation("josercl.androidutils:room:1.0.0")
implementation("josercl.androidutils:livedata:1.0.3")
implementation("josercl.androidutils:flow:1.0.3")
```